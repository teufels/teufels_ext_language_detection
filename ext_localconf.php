<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextlanguagedetectionhttpacceptlanguage',
	array(
		'HttpAcceptLanguage' => 'httpAcceptLanguage',
		
	),
	// non-cacheable actions
	array(
		'HttpAcceptLanguage' => 'httpAcceptLanguage',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextlanguagedetectioniptonation',
	array(
		'IpToNation' => 'ipToNation',
		
	),
	// non-cacheable actions
	array(
		'IpToNation' => 'ipToNation',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder