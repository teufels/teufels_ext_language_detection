<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Charset\CharsetConverter;
/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * HttpAcceptLanguageController
 */
class HttpAcceptLanguageController extends \TEUFELS\TeufelsExtLanguageDetection\Controller\AbstractController
{

    /**
     * httpAcceptLanguageRepository
     *
     * @var \TEUFELS\TeufelsExtLanguageDetection\Domain\Repository\HttpAcceptLanguageRepository
     * @inject
     */
    protected $httpAcceptLanguageRepository = NULL;
    
    /**
     * action httpAcceptLanguage
     *
     * @return void
     */
    public function httpAcceptLanguageAction()
    {
        $bDebug = 0;
        $bHttpAcceptLanguageSwitch = 0;
        /*
         * Actual sys_language_uid
         */
        
        $iSysLanguageUid = intval($GLOBALS['TSFE']->sys_language_uid);
        if ($bDebug) {
            print '<pre>';
            print '$iSysLanguageUid:<br>';
            var_dump($iSysLanguageUid);
            print '</pre>';
        }
        /* @var $aHttpAcceptLanguages \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\HttpAcceptLanguage[] */
        $aHttpAcceptLanguages = $this->httpAcceptLanguageRepository->findAll()->toArray();
        $aAvailableHttpAcceptLanguages = array();
        $aAvailableHttpAcceptLanguagesObj = array();
        foreach ($aHttpAcceptLanguages as $oHttpAcceptLanguage) {
            $sLanguageKey = strtolower(str_replace('_', '-', $oHttpAcceptLanguage->getTitle()));
            $aAvailableHttpAcceptLanguagesObj[$sLanguageKey] = $oHttpAcceptLanguage;
            $aAvailableHttpAcceptLanguages[] = $sLanguageKey;
        }
        if ($bDebug) {
            print '<pre>';
            print '$aAvailableHttpAcceptLanguages:<br>';
            var_dump($aAvailableHttpAcceptLanguages);
            print '</pre>';
        }
        $sPreferedLanguage = $this->prefered_language($aAvailableHttpAcceptLanguages, strtolower(GeneralUtility::getIndpEnv('HTTP_ACCEPT_LANGUAGE')));
        if ($bDebug) {
            print '<pre>';
            print '$sPreferedLanguage:<br>';
            var_dump($sPreferedLanguage);
            print '</pre>';
        }
        /*
         * $iSysLanguageUid != $aAvailableLanguagesTemp[$sLang]
         * => show language switcher
         */
        
        if (array_key_exists($sPreferedLanguage, $aAvailableHttpAcceptLanguagesObj)) {
            if ($iSysLanguageUid != $aAvailableHttpAcceptLanguagesObj[$sPreferedLanguage]->getSysLangUid()) {
                $bHttpAcceptLanguageSwitch = 1;
            }
        }
        if ($bDebug) {
            print '<pre>';
            print '$bHttpAcceptLanguageSwitch:<br>';
            var_dump($bHttpAcceptLanguageSwitch);
            print '</pre>';
        }
        if (isset($_COOKIE['LD']) && $_COOKIE['LD'] == 0) {
            $bHttpAcceptLanguageSwitch = 0;
            if ($bDebug) {
                print '<pre>';
                print '$bHttpAcceptLanguageSwitch:<br>';
                var_dump($bHttpAcceptLanguageSwitch);
                print '</pre>';
            }
        }
        $this->view->assign('bHttpAcceptLanguageSwitch', $bHttpAcceptLanguageSwitch);
        $this->view->assign('sHttpAcceptLanguage', $sPreferedLanguage);
        $this->view->assign('oHttpAcceptLanguage', $aAvailableHttpAcceptLanguagesObj[$sPreferedLanguage]);
    }
    
    /**
     * @param $available_languages
     * @param $http_accept_language
     * @param $default_language
     */
    protected function prefered_language($available_languages, $http_accept_language, $default_language = 'en')
    {
        $available_languages = array_flip($available_languages);
        $langs = array();
        preg_match_all('~([\\w-]+)(?:[^,\\d]+([\\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            //list($a, $b) = explode('-', $match[1]) + array('', '');
            $value = isset($match[2]) ? (double) $match[2] : 1.0;
            if (isset($available_languages[$match[1]])) {
                $langs[$match[1]] = $value;
                continue;
            }
        }
        if ($langs) {
            arsort($langs);
            return key($langs);
        } else {
            return $default_language;
        }
    }
    
    /**
     * Returns LanguageService
     *
     * @return \TYPO3\CMS\Lang\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

}