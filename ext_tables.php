<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextlanguagedetectionhttpacceptlanguage',
	'teufels_ext_language_detection :: httpAcceptLanguage'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextlanguagedetectioniptonation',
	'teufels_ext_language_detection :: '
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'teufels_ext_language_detection');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage', 'EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_csh_tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextlanguagedetection_domain_model_iptonation', 'EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_csh_tx_teufelsextlanguagedetection_domain_model_iptonation.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextlanguagedetection_domain_model_iptonation');
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder