<?php

namespace TEUFELS\TeufelsExtLanguageDetection\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\IpToNation.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class IpToNationTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\IpToNation
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\IpToNation();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getBackendTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getBackendTitle()
		);
	}

	/**
	 * @test
	 */
	public function setBackendTitleForStringSetsBackendTitle()
	{
		$this->subject->setBackendTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'backendTitle',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitle()
		);
	}

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle()
	{
		$this->subject->setTitle('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'title',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTargetReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTarget()
		);
	}

	/**
	 * @test
	 */
	public function setTargetForStringSetsTarget()
	{
		$this->subject->setTarget('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'target',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSysLangUidReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setSysLangUidForIntSetsSysLangUid()
	{	}

	/**
	 * @test
	 */
	public function getInfoReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getInfo()
		);
	}

	/**
	 * @test
	 */
	public function setInfoForStringSetsInfo()
	{
		$this->subject->setInfo('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'info',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLabelLangReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLabelLang()
		);
	}

	/**
	 * @test
	 */
	public function setLabelLangForStringSetsLabelLang()
	{
		$this->subject->setLabelLang('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'labelLang',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLabelSwitchReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLabelSwitch()
		);
	}

	/**
	 * @test
	 */
	public function setLabelSwitchForStringSetsLabelSwitch()
	{
		$this->subject->setLabelSwitch('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'labelSwitch',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getLabelDismissReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getLabelDismiss()
		);
	}

	/**
	 * @test
	 */
	public function setLabelDismissForStringSetsLabelDismiss()
	{
		$this->subject->setLabelDismiss('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'labelDismiss',
			$this->subject
		);
	}
}
