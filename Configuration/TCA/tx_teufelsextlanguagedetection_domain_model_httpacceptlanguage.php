<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage',
		'label' => 'backend_title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'backend_title,title,target,sys_lang_uid,info,label_switch,label_dismiss,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('teufels_ext_language_detection') . 'Resources/Public/Icons/tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, target, sys_lang_uid, info, label_switch, label_dismiss',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, backend_title, title, target, sys_lang_uid, info;;;richtext:rte_transform[mode=ts_links], label_switch, label_dismiss, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage',
				'foreign_table_where' => 'AND tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.pid=###CURRENT_PID### AND tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'backend_title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.backend_title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'target' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.target',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'sys_lang_uid' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.sys_lang_uid',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int,required'
			)
		),
		'info' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.info',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim,required',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rich_text_editor',
							'urlParameters' => array(
								'mode' => 'wizard',
								'act' => 'wizard_rte.php'
							)
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'label_switch' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.label_switch',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'label_dismiss' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.label_dismiss',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		
	),
);## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder