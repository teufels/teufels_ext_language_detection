
plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage {
	view {
		templateRootPaths.0 = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.persistence.storagePid}
	}
}

plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation {
	view {
		templateRootPaths.0 = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.persistence.storagePid}
	}
}

plugin.tx_teufelsextlanguagedetection._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-teufels-ext-language-detection table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-teufels-ext-language-detection table th {
		font-weight:bold;
	}

	.tx-teufels-ext-language-detection table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

page {
    includeCSS {
        teufels_ext_language_detection = EXT:teufels_ext_language_detection/Resources/Private/Assets/Less/index.less
        teufels_ext_language_detection.media = all
    }
}

##
## Language Detection
## via Browser settings
##
lib.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage = COA
lib.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtLanguageDetection
		pluginName = Teufelsextlanguagedetectionhttpacceptlanguage
		vendorName = TEUFELS
		controller = HttpAcceptLanguage
		action = httpAcceptLanguage
		settings =< plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.settings
		persistence =< plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.persistence
		view =< plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage.view
	}
}

##
## Language Detection
## via IP
##
lib.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation = COA
lib.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = TeufelsExtLanguageDetection
		pluginName = Teufelsextlanguagedetectioniptonation
		vendorName = TEUFELS
		controller = IpToNation
		action = ipToNation
		settings =< plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.settings
		persistence =< plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.persistence
		view =< plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation.view
	}
}